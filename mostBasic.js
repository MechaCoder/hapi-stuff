// to run theis as a sit run bash command `node mostBasic.js`

'use strict'

const Hapi = require('hapi');
const server = new Hapi.Server();

// server confi

server.connection(
  {port: 3000}
)

server.route({
  method: 'GET',
  path: '/',
  handler: function(request, reply){
    reply("hello World");
  }
});

server.route({
  method: 'GET',
  path: '/{name}',
  handler: function(request, reply){
    reply('Hello, ' + encodeURIComponent(request.params.name) + '!');
  }
})

server.route({
  method: 'GET',
  path:'/{name}/doing',
  handler: function(request, reply){
    reply("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

  }
})

server.start((err) => {
  if(err){
    throw err;
  }
  console.log( "server is running at; ", server.info.uri )
})
