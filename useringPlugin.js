// to run theis as a sit run bash command `node mostBasic.js`

'use strict'

const Hapi = require('hapi');
const server = new Hapi.Server();

// server confi

server.connection(
  {port: 3000}
)

server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }

    server.route({
        method: 'GET',
        path: '/hello/{name}',
        handler: function (request, reply) {
            reply.file('./public/hello.html');
            // reply("hello World");
        }
    });
});

server.start((err) => {
  if(err){
    throw err;
  }
  console.log( "server is running at; ", server.info.uri )
})
